const MDCTextField = mdc.textField.MDCTextField;
const textFields = [].map.call(
  document.querySelectorAll(".mdc-text-field"),
  function (el) {
    return new MDCTextField(el);
  }
);

const MDCCheckbox = mdc.checkbox.MDCCheckbox;
const checkboxes = [].map.call(
  document.querySelectorAll(".mdc-checkbox"),
  function (el) {
    return new MDCCheckbox(el);
  }
);

document.getElementById("signup-button").addEventListener("click", function () {
  // Get user input from text fields
  const fullName = document.getElementById("full-name").value;
  const username = document.getElementById("username").value;
  const password1 = document.getElementById("password").value;
  const password2 = document.getElementById("confirm-password").value;
  const birthdate = new Date(document.getElementById("birth-date").value);
  const age = parseInt(document.getElementById("age").value);

  // Checkboxes
  const legalCheckbox = document.getElementById("legal-checkbox").checked;
  const termsCheckbox = document.getElementById("terms-checkbox").checked;

  // Calculate the user's age based on the current date, considering year, month, and day
  const currentDate = new Date('October 19, 2023');
  const userYearOfBirth = birthdate.getFullYear();
  const userMonthOfBirth = birthdate.getMonth();
  const userDayOfBirth = birthdate.getDate();
  const currentYear = currentDate.getFullYear();
  const currentMonth = currentDate.getMonth();
  const currentDay = currentDate.getDate();
  let userAgeCalculated = currentYear - userYearOfBirth;
  let dobString = birthdate.toISOString();
  let dob = dobString.slice(0, 10);

  // Log user input
  console.log(`Full Name: ${fullName}`);
  console.log(`Username: ${username}`);
  console.log(`Enter Password: ${password1}`);
  console.log(`Confirm Password: ${password2}`);
  console.log(`Age: ${age}`);
  console.log(`Birth Date: ${dob}`);

  // Check and log checkbox status
  if (legalCheckbox && termsCheckbox) {
    console.log("The user has checked the legal checkbox");
    console.log("The user has checked the terms checkbox");
  } else if (legalCheckbox) {
    console.log("The user has checked the legal checkbox");
    console.log("The user has not checked the terms checkbox");
  } else if (termsCheckbox) {
    console.log("The user has checked the terms checkbox");
    console.log("The user has not checked the legal checkbox");
  } else {
    console.log("The user has not checked the legal checkbox");
    console.log("The user has not checked the terms checkbox");
  }

  // Adjust age based on the month and day of birth
if (currentMonth < userMonthOfBirth || (currentMonth === userMonthOfBirth && currentDay < userDayOfBirth)) {
  userAgeCalculated--; // User's birthday has not occurred this year yet
}

  // Check eligibility
  if (
    password1 === password2 &&
    birthdate instanceof Date &&
    (userAgeCalculated >= 13) &&
    (age >= 13) &&
    legalCheckbox &&
    termsCheckbox &&
    fullName.trim() !== '' &&
    username.trim() !== ''
  ) {
    console.log("The user is eligible");
    if (age !== userAgeCalculated) {
      console.log("The user is not likely to be good at math");
    } else {
      console.log("The user can figure out their age");
    }
  } else {
    console.log("The user is ineligible");
  }
});

